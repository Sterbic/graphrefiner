#!/bin/bash

if [ $# -ne 1 ] ; then
    echo -e "\nWrong number of arguments, program expects 1 argument.\n"
    exit -1
fi

graphPath="graphs/graph_${1}.fasta"
resultsPath="results/islands_${1}.fasta"

if [ ! -r "$graphPath" ] ; then
    echo -e "\nThe requested test is not available!\n"
    exit -1
fi

if [ -e "graphRefiner" -a -d "results" ] ; then
    ./graphRefiner "$graphPath" "$resultsPath"
    echo -e "\nTest completed successfully!"
    echo -e "\nResults analysis:"
    python3 "src/islandAnalyzer.py" "$graphPath" "$resultsPath"
else
    echo "Please run make before running tests!"
fi

