GraphRefiner
============

GraphRefiner is a module for directed weighted graph refinement in optical map assemblers.
Given an input graph, GraphRefiner will delete spurious elements and output a list of remaining isolated components, islands.

Author:

Luka Šterbić, luka.sterbic@gmail.com

Index:
---------------------

1. Dependencies

2. Installation

3. Usage

4. Tests


1) Dependencies
---------------------

To properly run GraphRefiner the following software is needed:

1. Bash shell
2. g++
3. GNU make
4. Python 3+ (for result analysis)

2) Installation
---------------------

To install GraphRefiner run `make install` in its root folder. This will build an executable named graphRefiner and create a folder named results where test results will be stored.
To uninstall GraphRefiner run `make clean` in its root folder. This will remove all files created by the install procedure and testing.


3) Usage
---------------------

To run GraphRefiner use the following command:

`./graphRefiner <graph_file> <islands_file>`

GraphRefiner expects two command line arguments:

1. The path to a .fasta file containing the graph definition

2. The path to a .fasta file that will contain the output islands

To analyze the results produced by GraphRefiner a python script is provided:

`./<GraphRefiner_root>/src/islandAnalizer.py <graph_file> <islands_file>` 


4) Tests
---------------------

To run a predefined test enter the following command while being placed in the GraphRefiner root folder:

`./tests/test.sh <X>`

`<X> = {1, 2, 3}`

The tests use graphs stored in `<GraphRefiner_root>/graphs/` and store results in `<GraphRefiner_root>/results/`.