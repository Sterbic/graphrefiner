
SRC_DIR = src
TEST_DIR = tests
RESULTS_DIR = results
NAME = graphRefiner

SRC := $(wildcard $(SRC_DIR)/*.cpp)
HEADERS := $(wildcard $(SRC_DIR)/*.h)
TESTS := $(wildcard $(TEST_DIR)/*.sh)
PYTHON := $(wildcard $(SRC_DIR)/*.py)

install: $(HEADERS) $(SRC)
	g++ -o $(NAME) $(SRC)
	chmod u+x $(TESTS) $(PYTHON)
	if [ ! -d "results" ]; then mkdir $(RESULTS_DIR); fi;

clean:
	if [ -d "results" ]; then rm -r $(RESULTS_DIR); fi;

	if [ -e "graphRefiner" ]; then rm $(NAME); fi;
