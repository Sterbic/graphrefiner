#include <cstdio>
#include <fstream>
#include <string>
#include <cassert>
#include <set>
#include <queue>
#include <stack>
#include <algorithm>
#include <cmath>

#include "Graph.h"

using namespace std;

struct sortPairFirstAscending {
    bool operator()(const std::pair<int,int> &a, const std::pair<int,int> &b) {
        return a.first < b.first;
    }
};

struct sortPairFirstDescending {
    bool operator()(const std::pair<int,int> &a, const std::pair<int,int> &b) {
        return a.first >= b.first;
    }
};

Graph::Graph(const char *path) {
    ifstream ifs;
    ifs.open(path);
    assert(ifs.good());
    
	nodeCount = 0;
	edgeCount = 0;
    labeled = false;
    foundDirect = false;
    maxLabel = 0;
    
    int first, second, weight;
    string line;
    
    while(!ifs.eof()) {
        getline(ifs, line);
        
        if(line.empty()) {
            break;
        } else if(line.find(">") == 0) {
            continue;
        }
        
        if(sscanf(line.c_str(), "%d\t%d\t%d", &first, &second, &weight) == 3) {
            edgeCount++;
            nodeCount = max(nodeCount, max(first, second));
        }
    }
    
	in = new vector<int>[nodeCount];
	assert(in);
    
	out = new vector<int>[nodeCount];
	assert(out);
    
	edges = new pair<int, int>[edgeCount];
	assert(edges);
    
    edgeWeights.reserve(edgeCount);
    illegalEdge = make_pair(-1, -1);
    
	confirmedNodes.reserve(nodeCount);
    fill_n(back_inserter(confirmedNodes), nodeCount, true);
    
    confirmedEdges.reserve(edgeCount);
    fill_n(back_inserter(confirmedEdges), edgeCount, false);
    
    groupLabels.reserve(nodeCount);
    fill(groupLabels.begin(), groupLabels.end(), 0);
    
    ifs.clear();
	ifs.seekg(0, ifs.beg);
    
    int edgeIndex = 0;
    while(!ifs.eof()) {
        getline(ifs, line);
        
        if(line.empty()) {
            break;
        } else if(line.find(">") == 0) {
            continue;
        }
        
        if(sscanf(line.c_str(), "%d\t%d\t%d", &first, &second, &weight) == 3) {
            first--;
            second--;
            
            out[first].push_back(edgeIndex);
            in[second].push_back(edgeIndex);
            
            edges[edgeIndex] = make_pair(first, second);
            edgeWeights[edgeIndex] = weight;
            
            edgeIndex++;
        }
    }
    
    for(int i = 0; i < nodeCount; ++i) {
        vector< pair<int, int> > indexWeightPairs;
        
        for(unsigned int edge = 0; edge < out[i].size(); ++edge) {
            int edgeIndex = out[i][edge];
            int weight = edgeWeights[edgeIndex];
            
            indexWeightPairs.push_back(make_pair(weight, edgeIndex));
        }
        
        sort(indexWeightPairs.begin(), indexWeightPairs.end(),
             sortPairFirstDescending());
        
        out[i].clear();
        
        for(unsigned int j = 0; j < indexWeightPairs.size(); ++j) {
            out[i].push_back(indexWeightPairs[j].second);
        }
    }
    
    ifs.close();
}

Graph::~Graph() {
	if(in != NULL) {
		delete[] in;
	}
	if(out != NULL) {
		delete[] out;
	}
	if(edges != NULL) {
		delete[] edges;
	}
    
	edgeWeights.clear();
	confirmedNodes.clear();
    confirmedEdges.clear();
    groupLabels.clear();
}

void Graph::deleteChimericNodes() {
    for(int i = 0; i < nodeCount; ++i) {
        if(isNodeChimeric(i)) {
            confirmedNodes[i] = false;
            deleteNode(i);
        }
    }
    
    deleteIsolatedNodes();
}

bool Graph::isNodeChimeric(int node) {
    // load all neighbouring nodes
    vector<int> neighbours;
    loadNeighbouringNodes(node, &neighbours);
    
    // init target set from neighbours vector
    set<int> target(neighbours.begin(), neighbours.end());
    
    // for each node do limit BFS to reach target set of nodes
    for(unsigned int i = 0; i < neighbours.size(); i++) {
        int startNode = neighbours[i];
        
        set<int> targetCpy = target;
        targetCpy.erase(startNode);
        
        if(targetBFS(startNode, node, &targetCpy)) {
            return false;
        }
    }
    
    return true;
}

bool Graph::targetBFS(int startNode, int banNode, set<int> *target) {
    // init queue to start node
    queue<int> open;
    open.push(startNode);
    
    // int closed set of nodes, ban node must not be expanded
    set<int> closed;
    closed.insert(banNode);
    
    // BFS limited by the number of expansions
    int expansions = 0;
    while(!open.empty()) {
        // get first element in open queue and add it to close set
        int n = open.front();
        open.pop();
        closed.insert(n);
        
        // erase from target set and check if empty
        target->erase(n);
        if(target->empty()) {
            return true;
        }
        
        // check expansion limit
        if(expansions > CHIMERIC_BFS_EXPANSION_LIMIT) {
            return false;
        }
        
        // get expanded nodes
        expansions ++;
        vector<int> expand;
        loadNeighbouringNodes(n, &expand);
        
        // add to queue all neighbouring nodes not in closed set
        for(unsigned int i = 0; i < expand.size(); ++i) {
            int expNode = expand[i];
            
            if(closed.find(expNode) == closed.end()) {
                open.push(expNode);
            }
        }
        
    }
    
    return false;
}

void Graph::printPaths(vector< vector<int> > *paths) {
    for(unsigned int path = 0; path < paths->size(); ++path) {
        for(unsigned int i = 0; i < paths->at(path).size(); ++i) {
            printf("%d, ", edges[paths->at(path).at(i)].second);
        }
        printf("%d\n", edges[paths->at(path).back()].first);
    }
}

void Graph::deleteOCFalseEdges() {
    for(int i = 0; i < nodeCount; ++i) {
        for(int j = 0; j < nodeCount; j++) {
            if(i == j) {
                continue;
            }
            
            // calculate theoretical max number of independent paths
            int maxPahts = min((int) out[i].size(), (int) in[j].size());
            if(maxPahts < 2) {
                continue;
            }
            
            // struct for indepentent paths between nodes i and j
            vector< vector<int> > paths;
            
            // clear ban set and found direct flag
            ban.clear();
            foundDirect = false;
            
            // use DFS to find paths
            findIndependentPaths(i, j, &paths, maxPahts);
            
            // calculate best cluster
            errorDistanceClustering(&paths);
        }
    }
    
    deleteIsolatedNodes();
}

void Graph::findIndependentPaths(int start, int end, vector< vector<int> > *paths, int maxPaths) {
    int pathCount = 0;
    
    while(true) {
        // init a new path
        path.clear();
        
        // if a new path was foun add it to paths
        if(recursiveDFS(start, -1, end, 0)) {
            paths->push_back(path);
            
            // if the path limit has been reached break
            pathCount++;
            if(pathCount == maxPaths) {
                break;
            }
        } else {
            break;
        }
    }
}

bool Graph::recursiveDFS(int node, int srcEdge, int target, int depth) {
    // if target reached return true and start path reconstruction
    if(node == target) {
        if(depth == 1) {
            if(foundDirect) {
                return false;
            } else {
                foundDirect = true;
            }
        }
        
        if(srcEdge != -1) {
            path.push_back(srcEdge);
        }
            
        return true;
    }
    
    // check depth limit
    if(depth >= FALSE_EDGES_DFS_DEPTH_LIMIT) {
        return false;
    }
    
    // iterate over outgoing edges
    for(unsigned int i = 0; i < out[node].size(); ++i) {
        int edgeIndex = out[node][i];
        int toNode = edges[edgeIndex].second;
        
        // check if new node is already banned
        if(ban.find(toNode) != ban.end()) {
            continue;
        }
        
        // recursivly search for target node
        if(recursiveDFS(toNode, edgeIndex, target, depth + 1)) {
            if(srcEdge != -1) {
                path.push_back(srcEdge);
                ban.insert(node);
            }
            
            return true;
        }
    }
    
    return false;
}

void Graph::errorDistanceClustering(vector< vector<int> > *paths) {
    vector< pair<int, int> > lenghtPathPairs;
    
    // create a vector of path lenghts and path indexes in *paths
    for(unsigned int path = 0; path < paths->size(); ++path) {
        int pathLenght = getPathLenght(&(paths->at(path)));
        
        lenghtPathPairs.push_back(make_pair(pathLenght, path));
    }
    
    // sort pairs by lenght ascending
    sort(lenghtPathPairs.begin(), lenghtPathPairs.end(),
         sortPairFirstAscending());
    
    // init best cluster size and spanning
    int bestSize = 0;
    int bestFrom = 0;
    int bestTo = -1;
    
    // find biggest cluster
    for(unsigned int i = 0; i < lenghtPathPairs.size(); ++i) {
        int dAlpha = lenghtPathPairs[i].first;
        int clusterSize = 1;
        int from = i;
        int to = i;
        
        // calculate cluster size
        double allowedDistance = sqrt(dAlpha) * FLASE_EDEGES_SIGMA;
        
        // check if longer paths can be added to cluster
        for(unsigned int j = i + 1; j < lenghtPathPairs.size(); ++j) {
            if(lenghtPathPairs[j].first - allowedDistance < dAlpha) {
                clusterSize++;
                to = j;
            } else {
                break;
            }
        }
        
        // check if shorter paths cab be added to cluster
        for(int j = i - 1; j >= 0; --j) {
            if(lenghtPathPairs[j].first + allowedDistance > dAlpha) {
                clusterSize++;
                from = j;
            } else {
                break;
            }
        }
        
        // check if current cluster is best so far
        if(clusterSize > bestSize) {
            bestSize = clusterSize;
            bestFrom = from;
            bestTo = to;
        }
    }
    
    // to continue the size of the best cluster must be at least 2
    if(bestSize < 2) {
        return;
    }
    
    // confirm paths of greatest cluster
    for(int i = bestFrom; i <= bestTo; ++i) {
        int pathIndex = lenghtPathPairs[i].second;
        confirmPath(paths, pathIndex);
    }
}

void Graph::confirmPath(vector< vector<int> > *paths, int path) {
    for(unsigned int i = 0; i < paths->at(path).size(); ++i) {
        int edge = paths->at(path).at(i);
        confirmedEdges[edge] = true;
    }
}

int Graph::findIndex(vector<int> *vec, int value) {
    for(unsigned int i = 0; i < vec->size(); i++) {
        if((*vec)[i] == value) {
            return i;
        }
    }
    return -1;
}

void Graph::deleteNode(int node) {
    for(unsigned int i = 0; i < in[node].size(); ++i) {
        int edgeIndex = in[node][i];
        confirmedEdges[edgeIndex] = false;
        
        int srcNode = edges[edgeIndex].first;
        edges[edgeIndex] = illegalEdge;
        
        int index = findIndex(&out[srcNode], edgeIndex);
        out[srcNode].erase(out[srcNode].begin() + index);
    }
    
    for(unsigned int i = 0; i < out[node].size(); ++i) {
        int edgeIndex = out[node][i];
        confirmedEdges[edgeIndex] = false;
        
        int dstNode = edges[edgeIndex].second;
        edges[edgeIndex] = illegalEdge;
        
        int index = findIndex(&in[dstNode], edgeIndex);
        in[dstNode].erase(in[dstNode].begin() + index);
    }
    
    confirmedNodes[node] = false;
    in[node].clear();
    out[node].clear();
}

int Graph::deleteIsolatedNodes() {
    int deleted = 0;
    
    for(int i = 0; i < nodeCount; ++i) {
        if(!hasNodeConfirmedEdges(i)) {
            deleteNode(i);
            deleted++;
        }
    }
    
    return deleted;
}

bool Graph::hasNodeConfirmedEdges(int node) {
    for(unsigned int i = 0; i < in[node].size(); ++i) {
        if(confirmedEdges[in[node][i]]) {
            return true;
        }
    }
    
    for(unsigned int i = 0; i < out[node].size(); ++i) {
        if(confirmedEdges[out[node][i]]) {
            return true;
        }
    }
    
    return false;
}

int Graph::getPathLenght(vector<int> *pathEdges) {
    int len = 0;
    
    for(unsigned int i = 0; i < pathEdges->size(); ++i) {
        len += edgeWeights[pathEdges->at(i)];
    }
    
    return len;
}

void Graph::initGroupLabels() {
    labeled = true;
    int currentLabel = 1;
    
    for(int i = 0; i < nodeCount; ++i) {
        if(groupLabels[i] == 0 && confirmedNodes[i]) {
            labeledDFS(i, currentLabel);
            currentLabel++;
        }
    }
    
    maxLabel = currentLabel - 1;
}

void Graph::labeledDFS(int startNode, int label) {
    // push first node on stack
    stack<int> open;
    open.push(startNode);
    
    // init closed set
    set<int> closed;
    
    while(!open.empty()) {
        // pop one node from stack, add it to closed set
        int n = open.top();
        open.pop();
        closed.insert(n);
        
        // if node is not labeled, label it
        if(groupLabels[n] == 0) {
            groupLabels[n] = label;
        }
        
        // load all neighboring nodes
        vector<int> expand;
        loadNeighbouringNodes(n, &expand);
        
        // add expanded nodes to stack if not in closed set
        for(unsigned int i = 0; i < expand.size(); ++i) {
            int expNode = expand[i];
            
            if(closed.find(expNode) == closed.end()) {
                open.push(expNode);
            }
        }
        
    }
}

int Graph::dumpIslandsToFile(const char *path) {
    if(!labeled) {
        initGroupLabels();
    }
    
    FILE *f = fopen(path, "w");
    assert(f);
    
    for(int label = 1; label <= maxLabel; ++label) {
        fprintf(f, ">%d\n", label);
        
        for(int i = 0; i < nodeCount; ++i) {
            if(groupLabels[i] == label && confirmedNodes[i]) {
                for(unsigned int edge = 0; edge < out[i].size(); ++edge) {
                    int outNode = edges[out[i][edge]].second + 1;
                    int weight = edgeWeights[out[i][edge]];
                    
                    if(confirmedEdges[out[i][edge]]) {
                        fprintf(f, "%d\t%d\t%d\n", i + 1, outNode, weight);
                    }
                }
            }
        }
        
        fprintf(f, "\n");
    }
    
    fclose(f);
    
    return maxLabel;
}

void Graph::loadOutputNodes(int node, vector<int> *vec) {
    for(unsigned int i = 0; i < out[node].size(); ++i) {
        if(confirmedEdges[out[node][i]]) {
            int pushNode = edges[out[node][i]].second;
            vec->push_back(pushNode);
        }
    }
}

void Graph::loadInputNodes(int node, vector<int> *vec) {
    for(unsigned int i = 0; i < in[node].size(); ++i) {
        if(confirmedEdges[in[node][i]]) {
            int pushNode = edges[in[node][i]].first;
            vec->push_back(pushNode);
        }
    }
}

void Graph::loadNeighbouringNodes(int node, vector<int> *vec) {
    loadInputNodes(node, vec);
    loadOutputNodes(node, vec);
}

void Graph::print() {
    printf("-------------------------\n");
    
    for(int i = 0; i < nodeCount; ++i) {
        if(!confirmedNodes[i]) {
            continue;
        }
        
        printf("Node %d:\n", i + 1);
        
        for(unsigned int j = 0; j < out[i].size(); ++j) {
            int edgeIndex = out[i][j];
            int nodeIndex = edges[edgeIndex].second + 1;
            
            if(confirmedEdges[edgeIndex]) {
                printf("\t--> [%d] --> %d\n", edgeWeights[edgeIndex], nodeIndex);
            }
        }
    }
    
    printf("-------------------------\n\n");
}

void Graph::printAll() {
    printf("-------------------------\n");
    
    for(int i = 0; i < nodeCount; ++i) {
        printf("Node %d:\n", i + 1);
        
        for(unsigned int j = 0; j < out[i].size(); ++j) {
            int edgeIndex = out[i][j];
            int nodeIndex = edges[edgeIndex].second + 1;
            
            printf("\t--> [%d] --> %d\n", edgeWeights[edgeIndex], nodeIndex);
        }
    }
    
    printf("-------------------------\n\n");
}

void Graph::dumpToFile(const char *path) {
    FILE *f = fopen(path, "w");
    assert(f);
    
    for(int i = 0; i < edgeCount; ++i) {
        if(!confirmedEdges[i]) {
            continue;
        }
        
        int first = edges[i].first;
        int second = edges[i].second;
        
        fprintf(f, "%d\t%d\t%d\n", first, second, edgeWeights[i]);
    }
    
    fclose(f);
}
