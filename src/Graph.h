#ifndef __grap_refiner__Graph__
#define __grap_refiner__Graph__

#define VERSION "v0.7"

#define FALSE_EDGES_DFS_DEPTH_LIMIT 50
#define CHIMERIC_BFS_EXPANSION_LIMIT 30

#define FLASE_EDEGES_SIGMA 0.306

#include <vector>
#include <set>

using namespace std;

class Graph {
private:
    int nodeCount;
    int edgeCount;
    
    bool labeled;
    int maxLabel;
    
    vector<int> *in;
    vector<int> *out;
    
    pair<int, int> *edges;
    vector<int> edgeWeights;
    pair<int, int> illegalEdge;
    
    vector<bool> confirmedNodes;
    vector<bool> confirmedEdges;
    
    set<int> ban;
    bool foundDirect;
    vector<int> path;
    
    vector<int> groupLabels;
    
    int findIndex(vector<int> *vec, int value);
    void deleteNode(int node);
    int deleteIsolatedNodes();
    int getPathLenght(vector<int> *pathEdges);
    bool hasNodeConfirmedEdges(int node);
    
    void findIndependentPaths(int start, int end, vector< vector<int> > *paths, int maxPaths);
    bool recursiveDFS(int node, int srcEdge, int target, int depth);
    void errorDistanceClustering(vector< vector<int> > *paths);
    void confirmPath(vector< vector<int> > *paths, int path);
    
    bool isNodeChimeric(int node);
    bool targetBFS(int startNode, int banNode, set<int> *target);
    
    void initGroupLabels();
    void labeledDFS(int startNode, int label);
    
    void loadOutputNodes(int node, vector<int> *vec);
    void loadInputNodes(int node, vector<int> *vec);
    void loadNeighbouringNodes(int node, vector<int> *vec);
    
    void printPaths(vector< vector<int> > *paths);
    
public:
	Graph(const char *path);
	~Graph();
    
    void deleteOCFalseEdges();
    void deleteChimericNodes();
    
    int dumpIslandsToFile(const char *path);
    
    void print();
    void printAll();
    void dumpToFile(const char *path);
};

#endif
