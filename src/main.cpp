#include <iostream>
#include <stdlib.h>

#include "Graph.h"

int main(int argc, char **argv) {
    if(argc != 3) {
        std::cout << ("Wrong number of arguments, program expects 2 arguments.\n");
        exit(-1);
    }
    
    printf("### Welcome to Graph Refiner %s\n\n", VERSION);
    
    const char *graphPath = argv[1];
    const char *islandsPath = argv[2];
    
    printf("> Constructing graph from file %s...", graphPath);
    Graph *graph = new Graph(graphPath);
    printf(" DONE\n\n");
    
    printf("Successfully loaded graph:\n");
    graph->printAll();
    
    printf("> Deleting orientation consistent false edges...");
    graph->deleteOCFalseEdges();
    printf(" DONE\n\n");
    
    printf("Resulting graph:\n");
    graph->print();
    
    printf("> Deleting chimeric nodes...");
    graph->deleteChimericNodes();
    printf(" DONE\n\n");
    
    printf("Resulting graph:\n");
    graph->print();
    
    printf("> Dumping islands to file %s...", islandsPath);
    int dumped = graph->dumpIslandsToFile(islandsPath);
    printf(" DONE\n\nIslands successfully dumped to file : %d\n\n", dumped);
    
    printf("> Deleting graph... ");
    delete graph;
    printf(" DONE\n");
}


