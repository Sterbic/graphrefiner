#!/usr/bin/env python3

import sys
import re

if(len(sys.argv) != 3):
    print("This script expects two arguments!")
    exit(-1)
    
class Graph:
    nodes = 0
    edges = 0
   
class Island:
    totalNodes = 0
    totalEdges = 0
    
    def __init__(self, label, nNodes, nEdges):
        self.label = label
        self.nNodes = nNodes
        self.nEdges = nEdges
    
graphFile = open(sys.argv[1], 'r', encoding="utf-8")

nodes = set()

for line in graphFile:
    if(line == "\n"):
        break
    
    if(line.startswith(">")):
        continue
    
    parts = re.split("\\s+", line);
    
    nodes.add(int(parts[0]))
    nodes.add(int(parts[1]))
    
    Graph.edges += 1

graphFile.close()
Graph.nodes = len(nodes)

print("#" * 35)
print("Input graph file: " + sys.argv[1])
print("Graph stats:")
print("\t>|V| = " + str(Graph.nodes))
print("\t>|E| = " + str(Graph.edges))
print("#" * 35)

islandFile = open(sys.argv[2], 'r', encoding="utf-8")
islands = []

label = 0
nodes = set()
edges = 0

for line in islandFile:
    if(line.startswith(">")):
        label = int(line[1:])
        continue
    
    if(len(line) == 0 or line == "\n"):
        islands.append(Island(label, len(nodes), edges))
        
        label = 0
        nodes.clear()
        edges = 0
        
        continue
    
    edges += 1
        
    parts = re.split("\\s+", line);
        
    nodes.add(int(parts[0]))
    nodes.add(int(parts[1]))
    
islandFile.close()

for island in islands:
    Island.totalNodes += island.nNodes
    Island.totalEdges += island.nEdges

print("Input islands file: " + sys.argv[2])
print("Islands stats:")
print("\t>|I| = " + str(len(islands)))
print("\t>|V| = " + str(Island.totalNodes))
print("\t>|E| = " + str(Island.totalEdges))
print("\t>dV = " + str(Island.totalNodes - Graph.nodes))
print("\t>dE = " + str(Island.totalNodes - Graph.edges))
print("#" * 35)